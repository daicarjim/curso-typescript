console.log("Hello World");

// types

var myString: string = "Hello World";
myString = 22 + "jaja";

var myNumber:number = 3;

var myBool:boolean = true;

var myVar:any = "hello";
myVar = false;

// Array

var Stringarray:string [] = ["team1", "team2", ""]
var Numberarray:number [] = [1, 2, 4]
var Booleanarray:boolean [] = [false, true, false]
var Anyarray:any [] = ["sopa", "2", "false"]

// tuple

var strNumTuple: [string, number];
strNumTuple = ["hola", 4];

// void, undefined, null

//let myVoid: void = undefined;
//let myNull: null = null;
//let myUndefinded: undefined = undefined;


// functions

function getsum (num1:number,num2:number):number{
    return num1 + num2;

}

let mySum = function(
  num1: number | string, 
  num2: number | string):number{

    if (typeof(num1) === 'string') {
        num1 = parseInt(num1);
    }
    if (typeof(num2) === 'string') {
        num2 = parseInt(num2);
    }
      return num1 + num2;

  }
        
function getName 
( firstname:string,
  lastname?:string):string
{
       return `${firstname} ${lastname}`;

}

getName("Pedro")

function myVoidFunction():void{
          return;
}

// Interfaces

interface Itodo {
     title: string;
     text: string;

}


function showToDo (todo: Itodo) {
    console.log(`${todo.title} - ${todo.text}`)
}

let myToDo:Itodo = {title: 'jiji', text: 'juju'}


showToDo(myToDo);


// Clases

class User {

    private name: string;
    public email: string;
    protected age: number;
    
    constructor(name:string, 
                email:string, 
                age:number){
      this.name = name;
      this.email = email;
      this.age = age;

    }

  register(){
   console.log(`${this.name} is register !`);

  }
  showMeage(){
     return this.age;


  }

  public AgeInYears(){
        return this.age + 'years';
  }

  public payInvoice(){

      console.log(`${this.name} paid invoice`)
  }

}



class Member extends User{
     id: number;

    constructor(id:number, 
                name:string, 
                email:string, 
                age:number){
      super(name, 
            email, 
            age);
      this.id = id;
    }
   payInvoice(){
    super.payInvoice();
   }
}


var john = new User('daimon','daimoncj@hotmail.com', 34 );
var lorenzo = new Member(8,'lorenzo', 'lorenzo@hotmail.com',77);
lorenzo.payInvoice();


document.write(myNumber.toString());



