var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
console.log("Hello World");
// types
var myString = "Hello World";
myString = 22 + "jaja";
var myNumber = 3;
var myBool = true;
var myVar = "hello";
myVar = false;
// Array
var Stringarray = ["team1", "team2", ""];
var Numberarray = [1, 2, 4];
var Booleanarray = [false, true, false];
var Anyarray = ["sopa", "2", "false"];
// tuple
var strNumTuple;
strNumTuple = ["hola", 4];
// void, undefined, null
//let myVoid: void = undefined;
//let myNull: null = null;
//let myUndefinded: undefined = undefined;
// functions
function getsum(num1, num2) {
    return num1 + num2;
}
var mySum = function (num1, num2) {
    if (typeof (num1) === 'string') {
        num1 = parseInt(num1);
    }
    if (typeof (num2) === 'string') {
        num2 = parseInt(num2);
    }
    return num1 + num2;
};
function getName(firstname, lastname) {
    return firstname + " " + lastname;
}
getName("Pedro");
function myVoidFunction() {
    return;
}
function showToDo(todo) {
    console.log(todo.title + " - " + todo.text);
}
var myToDo = { title: 'jiji', text: 'juju' };
showToDo(myToDo);
// Clases
var User = /** @class */ (function () {
    function User(name, email, age) {
        this.name = name;
        this.email = email;
        this.age = age;
    }
    User.prototype.register = function () {
        console.log(this.name + " is register !");
    };
    User.prototype.showMeage = function () {
        return this.age;
    };
    User.prototype.AgeInYears = function () {
        return this.age + 'years';
    };
    User.prototype.payInvoice = function () {
        console.log(this.name + " paid invoice");
    };
    return User;
}());
var Member = /** @class */ (function (_super) {
    __extends(Member, _super);
    function Member(id, name, email, age) {
        var _this = _super.call(this, name, email, age) || this;
        _this.id = id;
        return _this;
    }
    Member.prototype.payInvoice = function () {
        _super.prototype.payInvoice.call(this);
    };
    return Member;
}(User));
var john = new User('daimon', 'daimoncj@hotmail.com', 34);
var lorenzo = new Member(8, 'lorenzo', 'lorenzo@hotmail.com', 77);
lorenzo.payInvoice();
document.write(myNumber.toString());
